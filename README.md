# Frontend Mentor - In-browser markdown editor solution

This is a solution to the [In-browser markdown editor challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/inbrowser-markdown-editor-r16TrrQX9). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [How to improve it / What I miss](#how-to-improve-it/-what-i-miss)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- ✔ Create, Read, Update, and Delete markdown documents
- ✔ Name and save documents to be accessed as needed
- ✔ Edit the markdown of a document and see the formatted preview of the content
- ✔ View a full-page preview of the formatted content
- ✔ View the optimal layout for the app depending on their device's screen size
- ✔ See hover states for all interactive elements on the page
- ✔ **Bonus**: If you're building a purely front-end project, use localStorage to save the current state in the browser that persists when the browser is refreshed
- ⚠ **Bonus**: Build this project as a full-stack application ️

### Screenshots
![Desktop dark mode](./screenshots/desktop-dark.png)
![Desktop light mode](./screenshots/desktop-light.png)
![Menu](./screenshots/menu.png)

### Links

- [Solution URL](https://gitlab.com/ntoniolo42/inbrowser-markdown-editor)
- [Live Site](https://frontendmentor.inbrowser-markdown-editor.ntoniolo.wtf/#/)
- [DesignSystem](https://frontendmentor.inbrowser-markdown-editor.ntoniolo.wtf/#/design-system)

## My process

### Built with

Frontend:
- Mobile-first workflow
- [Vue 3](https://vuejs.org/)
- [Tailwind](https://tailwindcss.com/)
- [Tailwind Typography](https://tailwindcss.com/docs/typography-plugin)
- [Vite](https://vitejs.dev/)
- [Markdown It](https://github.com/markdown-it/markdown-it)
- [highlight,js](https://highlightjs.org/)

Backend:
- [Supabase](https://supabase.com/), Auth, Database

### How to improve it / What I miss

- Functional summary on Preview
- Sync scroll between Markdown and Preview
- Match width of input file name
- Obviously, make ARIA
- More animation

### What I learned

Configure the Tailwind Typography plugin. A great plugin who save me a loooooot of time !
You can apply a beautiful default style on any HTML, very useful for rendered HTML.

I discover the `Intl` API ! Nothing fancy but, a great news.

More exploration of Vue3 with [state management](https://vuejs.org/guide/scaling-up/state-management.html#simple-state-management-with-reactivity-api) made by a [composable](https://vuejs.org/guide/reusability/composables.html#what-is-a-composable).
Very simple State Manage with one composable.

Custom scrollbar.

### Continued development

- I need to explore more about CSS. I handle only very basic things, i need to see deeper.
- Start learning ARIA, and also some good HTML practices

### Useful resources

- [Markdown Test](https://github.com/mxstbr/markdown-test-file/edit/master/TEST.md) - A Markdown for testing more advanced behavior
- [Tailwind Typography plugin](https://play.tailwindcss.com/uj1vGACRJA?layout=preview) - What can you expect about Tailwind Typography
- [Tailwind Typography style](https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js) - This is how Tailwind Typography is built, and how you can add/override styles on your config file.
- [Intl](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Intl) Learn more about Internationalization API


## Author

- Website - [ntoniolo.wtf](https://ntoniolo.wtf)
- Frontend Mentor - [@On-Jin](https://www.frontendmentor.io/profile/On-Jin)

