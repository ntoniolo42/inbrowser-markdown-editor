import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from "./router/index";
import {initTheme} from "./updateTheme";

createApp(App)
    .use(router)
    .mount('#app');

initTheme();
