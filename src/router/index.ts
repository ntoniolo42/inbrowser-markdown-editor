import {createRouter, createWebHashHistory} from 'vue-router';
import DesignSystem from "/src/DesignSystem.vue";
import HomePage from '/src/HomePage.vue';

const routes = [
    {
        path: '/',
        name: 'HomePage',
        component: HomePage
    },
    {
        path: '/design-system',
        name: 'DesignSystem',
        component: DesignSystem
    }
];
const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
