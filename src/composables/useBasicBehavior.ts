import {ref, onMounted, onUnmounted, computed, ComputedRef, Ref, UnwrapRef} from 'vue'
import {FileData} from "../FileData";
import {User} from "@supabase/supabase-js";
import {supabase} from "../supabase";

const isSeePreview = ref(false);
const isMenuOpen = ref(false);
const isModalOpen = ref(false);
const isLoginSignInOpen = ref(false);
const isLogLoad = ref(false);

const filesData = ref<FileData[]>([]);
const currentFileDataIndex = ref<number>(0);

const user = ref<User | null>(null);

user.value = supabase.auth.user();

async function getFilesDataFromSupabase() {
    let {data, error, status} = await supabase.from("documents").select("*");
    if (error)
        throw error;
    filesData.value.concat(data as FileData[]);
}

async function saveFilesDataToSupabase() {
    let needGetFreshDataFromApi = false;
    let filesDataLocal = filesData.value.filter(fileData => fileData.id === -1);
    let filesDataSupabase = filesData.value.filter(fileData => fileData.id !== -1);
    console.log(filesDataLocal)
    filesDataLocal.forEach(f => console.log(f))
    if (filesDataLocal.length) {
        filesDataLocal.forEach(fileData => {
            // @ts-ignore
            delete fileData.id;
            fileData.user_id = user.value!.id
        });
        console.log("Save local FileDatas on Supabase")
        let {data, error, status} = await supabase.from("documents").insert(filesDataLocal);
        if (error)
            throw error;
        needGetFreshDataFromApi = true;
    }
    {
        console.log("Update FileDatas on Supabase")
        let {data, error, status} = await supabase.from("documents").update(filesDataSupabase);
        if (error)
            throw error;
    }

    clearLocalStorage();
    if (needGetFreshDataFromApi) {
        let {data, error, status} = await supabase.from("documents").select('*');
        if (error)
            throw error;
        filesData.value = data as FileData[];
    }

}

supabase.auth.onAuthStateChange((event, session) => {
    console.log("Change State")

    user.value = session ? session.user : null;

    if (event == 'SIGNED_IN') {
        getFilesDataFromSupabase();
    }
    if (event == 'SIGNED_OUT') {
        filesData.value = filesData.value.filter(fileData => fileData.id !== -1);
    }
});


const currentFileData = computed<FileData>(() => {
    if (!(currentFileDataIndex.value <= filesData.value.length - 1)) {
        getFilesData();
    }
    return filesData.value[currentFileDataIndex.value]
});

const filesDataKey = 'filesData';

function switchSeePreview() {
    isSeePreview.value = !isSeePreview.value;
}

async function deleteCurrentFileData() {
    const fileDataToDelete = filesData.value.splice(currentFileDataIndex.value, 1);
    if (user.value && fileDataToDelete.length && fileDataToDelete[0].id !== -1) {
        console.log('Delete on Supapabse', fileDataToDelete[0].id)
        let {data, error, status} = await supabase.from("documents").delete().match({id: fileDataToDelete[0].id});
        if (error)
            throw error;
    }

    saveFilesData();
    getFilesData();
    currentFileDataIndex.value = 0;
}

function saveFilesData() {
    if (user.value)
        saveFilesDataToSupabase();
    else
        localStorage.setItem(filesDataKey, JSON.stringify(filesData.value));
}

function addNewDocument() {
    filesData.value.push(new FileData());
    currentFileDataIndex.value = filesData.value.length - 1;
}

function clearLocalStorage() {
    localStorage.removeItem(filesDataKey);
}

function getFilesData() {
    filesData.value = [];
    const stringFilesData: string | null = localStorage.getItem(filesDataKey);

    if (stringFilesData === null) {
        addNewDocument();
    } else {
        filesData.value = JSON.parse(stringFilesData) as FileData[];
        if (!filesData.value.length) {
            addNewDocument();
        }
    }
}

function setCurrentFileDataByIndex(index: number) {
    if (index >= 0 && index < filesData.value.length) {
        currentFileDataIndex.value = index;
    }
}

export function useBasicBehavior() {

    if (!filesData.value.length) {
        getFilesData();
    }

    return {
        user,
        isLogLoad,
        isLoginSignInOpen,
        isModalOpen,
        isSeePreview,
        currentFileData,
        filesData,
        currentFileDataIndex,
        deleteCurrentFileData,
        setCurrentFileDataByIndex,
        addNewDocument,
        saveFilesData,
        isMenuOpen,
        switchSeePreview
    }
}
