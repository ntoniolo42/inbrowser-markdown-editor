export enum Theme {
    Light = 'light',
    Dark = 'dark'
}

export function initTheme () {
    updateTheme();
}

export function switchTheme () {
    setTheme(localStorage.theme === 'dark' ? Theme.Dark : Theme.Light);
}

export function setTheme (to: Theme) {
    localStorage.theme = to ;
    updateTheme();
}

function updateTheme() {
    if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        document.documentElement.classList.add('dark')
    } else {
        document.documentElement.classList.remove('dark')
    }
}
