/** @type {import('tailwindcss').Config} */
const round = (num) =>
    num
        .toFixed(7)
        .replace(/(\.[0-9]+?)0+$/, '$1')
        .replace(/\.0$/, '')
'em = (px) => `${round(px / 16)}rem'
const em = (px, base) => `${round(px / base)}em`

module.exports = {
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    darkMode: 'class',
    theme: {
        extend: {
            fontFamily: {
                commissioner: ['Commissioner', 'sans-serif'],
                roboto: ['Roboto', 'sans-serif'],
                'roboto-mono': ['Roboto Mono', 'monospace'],
                'roboto-slab': ['Roboto Slab', 'serif'],
            },
            colors: {
                palette: {
                    900: "#151619",
                    800: "#1D1F22",
                    700: "#2B2D31",
                    600: "#35393F",
                    500: "#5A6069",
                    400: "#7C8187",
                    300: "#C1C4CB",
                    200: "#E4E4E4",
                    100: "#F5F5F5",
                    50: "#FFFFFF",
                },
                'orange-high': "#E46643",
                'orange-light': "#F39765",
            },

            typography: (theme) => {
                return ({
                    light: {
                        css: {
                            color: theme('colors.palette[400]'),
                            '--tw-prose-counters': theme('colors.palette[400]'),
                            '--tw-prose-links': theme('colors.palette[500]'),

                            '--tw-prose-headings': theme('colors.palette[600]'),
                            '--tw-prose-bold': theme('colors.palette[600]'),

                            '--tw-prose-bullets': theme('colors.orange-high'),
                            '--tw-prose-quote-borders': theme('colors.orange-high'),

                            '--tw-prose-code': theme('colors.palette[600]'),
                            '--tw-prose-pre-code': theme('colors.palette[600]'),
                            '--tw-prose-pre-bg': theme('colors.palette[100]'),
                            p: {
                                fontSize: '0.875rem'
                            },
                            li: {
                                fontSize: '0.875rem'
                            },
                            'code::before': {
                                display: 'none',
                            },
                            'code::after': {
                                display: 'none',
                            },
                            blockquote: {
                                paddingTop: "0.75rem",
                                paddingBottom: "0.75rem",
                                paddingRight: "1rem",
                                paddingLeft: "1.25rem",
                                color: theme('colors.palette[600]'),
                                backgroundColor: theme('colors.palette[100]'),
                                borderLeftWidth: '0.25em',
                            },
                            'blockquote a': {
                                color: theme('colors.palette[600]'),
                            },
                            'blockquote p:first-of-type::before': {
                                display: 'none',
                            },
                            'blockquote p:last-of-type::after': {
                                display: 'none',
                            },
                            h1: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '2rem'
                            },
                            h2: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '300',
                                fontSize: '1.75rem'
                            },
                            h3: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '1.5rem'
                            },
                            h4: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '1.25rem'
                            },
                            h5: {
                                fontFamily: `Roboto Slab, serif`,
                                color: theme('colors.palette[600]'),
                                fontWeight: '700',
                                fontSize: '1rem'
                            },
                            h6: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '0.875rem',
                                color: theme('colors.orange-high'),
                            },
                        }
                    },
                    dark: {
                        css: {
                            color: theme('colors.palette[300]'),
                            '--tw-prose-counters': theme('colors.palette[300]'),
                            '--tw-prose-links': theme('colors.palette[200]'),

                            '--tw-prose-headings': theme('colors.palette[50]'),
                            '--tw-prose-bold': theme('colors.palette[50]'),

                            '--tw-prose-bullets': theme('colors.orange-high'),
                            '--tw-prose-quote-borders': theme('colors.orange-high'),

                            '--tw-prose-code': theme('colors.palette[50]'),
                            '--tw-prose-pre-code': theme('colors.palette[50]'),
                            '--tw-prose-pre-bg': theme('colors.palette[700]'),
                            p: {
                              fontSize: '0.875rem'
                            },
                            li: {
                                fontSize: '0.875rem'
                            },
                            'code::before': {
                                display: 'none',
                            },
                            'code::after': {
                                display: 'none',
                            },
                            blockquote: {
                                paddingTop: "0.75rem",
                                paddingBottom: "0.75rem",
                                paddingRight: "1rem",
                                paddingLeft: "1.25rem",
                                color: theme('colors.palette[50]'),
                                backgroundColor: theme('colors.palette[700]'),
                                borderLeftWidth: '0.25em',
                            },
                            'blockquote a': {
                                color: theme('colors.palette[50]'),
                            },
                            'blockquote p:first-of-type::before': {
                                display: 'none',
                            },
                            'blockquote p:last-of-type::after': {
                                display: 'none',
                            },
                            h1: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '2rem'
                            },
                            h2: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '300',
                                fontSize: '1.75rem'
                            },
                            h3: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '1.5rem'
                            },
                            h4: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '1.25rem'
                            },
                            h5: {
                                fontFamily: `Roboto Slab, serif`,
                                color: theme('colors.palette[50]'),
                                fontWeight: '700',
                                fontSize: '1rem'
                            },
                            h6: {
                                fontFamily: `Roboto Slab, serif`,
                                fontWeight: '700',
                                fontSize: '0.875rem',
                                color: theme('colors.orange-high'),
                            },
                        }
                    }
                });
            }
        },
    },
    plugins: [
        require('@tailwindcss/typography'),
    ],
}
